import 'package:flutter/material.dart';
import 'package:getbeyond/screens/login/bottom.dart';
import 'package:getbeyond/screens/login/contact_screen/connection.dart';
import 'package:getbeyond/screens/login/contact_screen/group.dart';
import 'package:getbeyond/screens/login/focus_screens/add_link.dart';
import 'package:getbeyond/screens/login/focus_screens/home.dart';
import 'package:getbeyond/screens/login/focus_screens/setup.dart';
import 'package:getbeyond/screens/login/focus_screens/today_meeting.dart';
import 'package:getbeyond/screens/login/focus_screens/travel_recom.dart';
import 'package:getbeyond/screens/login/focus_screens/week.dart';

class PageSystem extends StatefulWidget {
  PageSystem({Key? key}) : super(key: key);

  @override
  State<PageSystem> createState() => _PageSystemState();
}

class _PageSystemState extends State<PageSystem> {
  late AddLink addLink;
  late Home home;
  late SetUp setUp;
  late Connection connection;
  late TodayMeeting todayMeeting;
  late TravelRecommendation travelRecommendation;
  late Week week;
  late Group group;
  late Bottom bottom;
  List<Widget> pages = [];
  @override
  void initState() {
    pages = [
      AddLink(),
      Home(),
      SetUp(),
      TodayMeeting(),
      Week(),
      TravelRecommendation(),
      Bottom(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView.builder(
      itemCount: pages.length,
      itemBuilder: (context, index) {
        return pages[index];
      },
    ));
  }
}
