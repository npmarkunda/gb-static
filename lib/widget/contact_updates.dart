import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getbeyond/model/contactupdates.dart';

List<ContactUpdates> getUpdates() {
  List<ContactUpdates> myCategories = [];
  ContactUpdates categorieModel;

  //1
  categorieModel = new ContactUpdates();
  categorieModel.detail = "Loving the thoughtful";
  categorieModel.tag = "#personalCRM";
  categorieModel.id = "@getbeyond.ai";
  categorieModel.image1 = "images/color_twitter.PNG";
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  return myCategories;
}
