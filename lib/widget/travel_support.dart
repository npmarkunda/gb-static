import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getbeyond/model/travel_model.dart';
import 'package:getbeyond/screens/login/focus_screens/travel_recom.dart';

List<TravelModel> getTravelRecommendations() {
  List<TravelModel> myCategories = [];
  TravelModel categorieModel;

  //1
  categorieModel = new TravelModel();
  categorieModel.name = "Anjali Human";
  categorieModel.company = "IBM INTEGRATION  ";
  categorieModel.time = "07 JUN\n2020";
  categorieModel.icon = Icon(
    Icons.flight,
    size: 30.0,
    color: Color(0xFF5992AD),
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  //2
  categorieModel = new TravelModel();
  categorieModel.name = "Amit Khannna";
  categorieModel.company = "TESLA SALES CALL";
  categorieModel.time = "06 JUN\n2020";
  categorieModel.icon = Icon(
    Icons.flight,
    size: 30.0,
    color: Color(0xFF5992AD),
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  //4

  return myCategories;
}
