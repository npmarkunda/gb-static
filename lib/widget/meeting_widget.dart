import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getbeyond/model/meeting_today.dart';

List<MeetingToday> getCategories() {
  List<MeetingToday> myCategories = [];
  MeetingToday categorieModel;

  //1
  categorieModel = new MeetingToday();
  categorieModel.name = "Anjali Human";
  categorieModel.company = "IBM INTEGRATION";
  categorieModel.time = "07.00 \n AM";
  categorieModel.icon = Icon(
    Icons.arrow_forward_ios_rounded,
    color: Color(0xFF5992AD),
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  //2
  categorieModel = new MeetingToday();
  categorieModel.name = "Amit Khannna";
  categorieModel.company = "TESLA SALES CALL";
  categorieModel.time = "12:30 \n PM";
  categorieModel.icon = Icon(
    Icons.arrow_forward_ios_rounded,
    color: Color(0xFF5992AD),
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);
  //3
  categorieModel = new MeetingToday();
  categorieModel.name = "Amit Khannna";
  categorieModel.company = "TESLA SALES CALL";
  categorieModel.time = "12:30 \n PM";
  categorieModel.icon = Icon(
    Icons.arrow_forward_ios_rounded,
    color: Color(0xFF5992AD),
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  //4

  return myCategories;
}
