import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getbeyond/model/reminders.dart';

List<Reminders> getReminders() {
  List<Reminders> myCategories = [];
  Reminders categorieModel;

  //1
  categorieModel = new Reminders();
  categorieModel.name = "Robert James";
  categorieModel.company = "BIRTHDAY TODAY";

  categorieModel.icon = Icon(
    Icons.cake_outlined,
    size: 30.0,
    color: Color(0xFF5992AD),
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  return myCategories;
}
