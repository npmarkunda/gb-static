import 'package:getbeyond/model/group_model.dart';
import 'package:getbeyond/screens/login/contact_screen/group.dart';

List<GroupModel> getGroupModel() {
  List<GroupModel> myCategories = [];
  GroupModel categorieModel;

  //1
  categorieModel = new GroupModel();
  categorieModel.work = "Co-investing(Angel)";
  categorieModel.members = "32 MEMBERS";
  myCategories.add(categorieModel);

  categorieModel = new GroupModel();
  categorieModel.work = "Weekday drinks";
  categorieModel.members = "04 MEMBERS";
  myCategories.add(categorieModel);
  return myCategories;
}
