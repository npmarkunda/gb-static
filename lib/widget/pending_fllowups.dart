import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:getbeyond/model/followups.dart';

List<FollowUps> getFollowups() {
  List<FollowUps> myCategories = [];
  FollowUps categorieModel;

  //1
  categorieModel = new FollowUps();
  categorieModel.detail =
      "How was yesterday's coffee \nmeet with Sofia. Would you \nlike to add a note?";
  categorieModel.icon = Icon(
    Icons.coffee_rounded,
    color: Color(0xFF5992AD),
    size: 30.0,
  );
  categorieModel.image = "images/pic.jpg";
  myCategories.add(categorieModel);

  return myCategories;
}
