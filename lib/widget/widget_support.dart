import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';

class AppWidget {
  static Widget createAppLogo(String getTheme) {
    return getTheme == "Black"
        ? Container(
            alignment: Alignment.center,
            child: Image.asset(
              "images/logo_white.png",
              fit: BoxFit.fill,
            ),
          )
        : Container(
            child: Image.asset(
              "images/logo.png",
              fit: BoxFit.fill,
            ),
          );
  }

  static TextStyle boldTextFeildStyle(String getTheme) {
    return getTheme == "Black"
        ? TextStyle(
            wordSpacing: 1.0,
            letterSpacing: 1.0,
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 16.0)
        : TextStyle(
            wordSpacing: 1.0,
            letterSpacing: 1.0,
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: 16.0);
  }

  static TextStyle boldTextwithColorFeildStyle(String getTheme) {
    return getTheme == "Black"
        ? TextStyle(
            color: Color(0xFF19A1E5),
            fontWeight: FontWeight.bold,
            fontSize: 19.0)
        : TextStyle(
            color: Color(0xFF5992AD),
            fontWeight: FontWeight.bold,
            fontSize: 19.0);
  }

  static TextStyle simpleTextFeildStyle(String getTheme) {
    return getTheme == "Black"
        ? TextStyle(
            wordSpacing: 1.0,
            letterSpacing: 1.0,
            color: Colors.white54,
            fontSize: 14.0,
            fontWeight: FontWeight.w500)
        : TextStyle(
            wordSpacing: 1.0,
            letterSpacing: 1.0,
            color: Colors.black54,
            fontSize: 14.0,
            fontWeight: FontWeight.w500);
  }

  static TextStyle lightTextFeildStyle() {
    return TextStyle(
      wordSpacing: 1.0,
      letterSpacing: 1.0,
      color: Color(0xFFBDBDBD),
      fontSize: 17.0,
    );
  }

  static Widget createAppButton(String name, String getTheme) {
    return getTheme == "Black"
        ? Container(
            width: 140,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Color(0xFF19A1E5),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Text(
                name,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0),
              ),
            ),
          )
        : Container(
            width: 140,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Color(0xFF5992AD),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Text(
                name,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0),
              ),
            ),
          );
  }

  static Widget createSocialMediaButton(
      String? image, String name, String getheme) {
    return getheme == "Black"
        ? Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white54,
                ),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                image == ""
                    ? Container()
                    : Container(
                        height: 35,
                        width: 35,
                        child: Image.asset(
                          image!,
                          fit: BoxFit.fill,
                        ),
                      ),
                SizedBox(
                  width: 20.0,
                ),
                Text(name, style: AppWidget.boldTextFeildStyle("Black")),
                Spacer(),
                Icon(Icons.add, color: Color(0xFF19A1E5), size: 30.0)
              ],
            ),
          )
        : Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black54,
                ),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                Container(
                  height: 40,
                  width: 40,
                  child: Image.asset(
                    image!,
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  width: 20.0,
                ),
                Text(name, style: AppWidget.boldTextFeildStyle("White")),
                Spacer(),
                Icon(Icons.add, color: Color(0xFF5992AD), size: 30.0)
              ],
            ),
          );
  }

  static Widget createSearchBar(String getTheme) {
    return getTheme == "Black"
        ? Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Color(0XFF1F2023),
                borderRadius: BorderRadius.circular(10)),
            child: TextField(
              decoration: InputDecoration(
                  hintText: 'Search Contacts',
                  border: InputBorder.none,
                  hintStyle: AppWidget.boldTextFeildStyle("Black"),
                  suffixIcon: Icon(
                    Icons.search,
                    size: 30.0,
                    color: Color(0xFF19A1E5),
                  )),
            ),
          )
        : Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Color(0XFFF3F7F9),
                borderRadius: BorderRadius.circular(10)),
            child: TextField(
              decoration: InputDecoration(
                  hintText: 'Search Contacts',
                  border: InputBorder.none,
                  hintStyle: AppWidget.boldTextFeildStyle("White"),
                  suffixIcon: Icon(
                    Icons.search,
                    size: 30.0,
                    color: Color(0xFF5992AD),
                  )),
            ),
          );
  }

  static Widget createRecommendationBar(
      String name, Icon? icon, int? value, bool showIcon, String getTheme) {
    return getTheme == "Black"
        ? Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                name,
                style: AppWidget.boldTextFeildStyle("Black"),
              ),
              showIcon
                  ? Container(
                      width: 70,
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                          color: Color(0XFF1F2023),
                          borderRadius: BorderRadius.circular(10)),
                      child: icon,
                    )
                  : Container(
                      width: 60,
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                          color: Color(0XFF1F2023),
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                        child: Text(
                          value!.toString(),
                          style: TextStyle(
                              color: Color(
                                0xFF19A1E5,
                              ),
                              fontSize: 25.0),
                        ),
                      ),
                    )
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                name,
                style: AppWidget.boldTextFeildStyle("White"),
              ),
              showIcon
                  ? Container(
                      width: 70,
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                          color: Color(0XFFF3F7F9),
                          borderRadius: BorderRadius.circular(10)),
                      child: icon,
                    )
                  : Container(
                      width: 70,
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                          color: Color(0XFFF3F7F9),
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(
                        child: Text(
                          value!.toString(),
                          style: TextStyle(
                              color: Color(
                                0xFF5992AD,
                              ),
                              fontSize: 25.0),
                        ),
                      ),
                    )
            ],
          );
  }

  static Widget createFocusCard(String name, String getTheme) {
    return getTheme == "Black"
        ? Container(
            height: 120,
            width: 150,
            decoration: BoxDecoration(
                color: Color(0XFF1F2023),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "0",
                    style: TextStyle(fontSize: 50.0, color: Color(0xFF19A1E5)),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(name, style: AppWidget.boldTextFeildStyle("Black")),
                ],
              ),
            ),
          )
        : Container(
            height: 120,
            width: 150,
            decoration: BoxDecoration(
                color: Color(0XFFF3F7F9),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "0",
                    style: TextStyle(fontSize: 50.0, color: Color(0xFF5992AD)),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(name, style: AppWidget.boldTextFeildStyle("White")),
                ],
              ),
            ),
          );
  }

  static Widget getYourProfile(String getTheme) {
    return Row(
      children: [
        Container(
          width: 50,
          height: 50,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Image.asset(
              "images/pic.jpg",
              fit: BoxFit.fill,
            ),
          ),
        ),
        SizedBox(
          width: 10.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            getTheme == "Black"
                ? Text(
                    "Sudip Dutta",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  )
                : Text(
                    "Sudip Dutta",
                    style: AppWidget.boldTextFeildStyle("White"),
                  ),
            getTheme == "Black"
                ? Text("BANGALORE, INDIA",
                    style: AppWidget.simpleTextFeildStyle("Black"))
                : Text("BANGALORE, INDIA",
                    style: AppWidget.simpleTextFeildStyle("White"))
          ],
        ),
        SizedBox(
          width: 110.0,
        ),
        getTheme == "Black"
            ? Icon(Icons.star, color: Color(0xFF19A1E5), size: 30.0)
            : Icon(Icons.star, color: Color(0xFF5992AD), size: 30.0),
      ],
    );
  }

  static Widget getYourContainer(String name, String? colorping) {
    return Container(
      padding: EdgeInsets.all(13),
      decoration: BoxDecoration(
          border:
              Border.all(color: colorping == "red" ? Colors.red : Colors.white),
          borderRadius: BorderRadius.circular(6)),
      child: Text(
        name,
        style: AppWidget.boldTextFeildStyle("Black"),
      ),
    );
  }

  static Widget createSettingBar(String name) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  color: Color(0xFFAEECD0),
                  borderRadius: BorderRadius.circular(30)),
              child: Icon(Icons.done),
            ),
            Text(
              name,
              style: AppWidget.boldTextFeildStyle("Black"),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.blue,
            )
          ],
        ),
        SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}
