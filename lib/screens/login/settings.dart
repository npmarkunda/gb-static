import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Settings extends StatefulWidget {
  Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 30.0,
              ),
              AppWidget.createAppLogo("Black"),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Hi Paul",
                style: AppWidget.boldTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "You are 40 % closer to experiencing magic",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 20.0,
              ),
              AppWidget.createSettingBar("Update your personal details"),
              AppWidget.createSettingBar("Integrate Twitter"),
              AppWidget.createSettingBar("Integrate LinkedIn"),
              AppWidget.createSettingBar("Integrate Mail"),
              Divider(
                color: Colors.white,
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Help and feedback",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                  Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: Colors.blue,
                  )
                ],
              )
            ],
          )),
    );
  }
}
