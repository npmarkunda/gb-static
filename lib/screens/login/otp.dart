import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:getbeyond/model/session_manage.dart';
import 'package:getbeyond/screens/login/bottomnav.dart';
import 'package:getbeyond/screens/login/email.dart';
import 'package:getbeyond/screens/login/name.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';
import 'package:http/http.dart' as http;

// class Message {
//   late String message;

//   Message({required this.message});
// }

class OTP extends StatefulWidget {
  String email;
  OTP({required this.email});

  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  String? getheme;

  final _formKey = GlobalKey<FormState>();
  TextEditingController userOtp = new TextEditingController();
  getTheTheme() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doOnThisLaunch() async {
    await getTheTheme();
    setState(() {});
  }

  late Map<String, dynamic> credentials;

  late String message;

  verifyOtp() async {
    try {
      var response = await http.post(
          Uri.parse(
            "https://api-dev.getbeyond.ai/user/verify/otp",
          ),
          body: {
            "emailId": widget.email,
            "otp": userOtp.text,
          });

      Map<String, dynamic> jsonData = jsonDecode(response.body);
      message = jsonData["msg"];
      credentials = jsonData["credentials"];
      SessionManagement().getToken(credentials["token"]);

      if (message == "OTP verified") {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Bottomnav()));
      } else
        Fluttertoast.showToast(
            msg: "Invalid OTP",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
    } catch (e) {
      print(e);
    }
  }

  sendOtpAgain() async {
    try {
      var response = await http.post(
          Uri.parse(
            "https://api-dev.getbeyond.ai/user/resend/otp",
          ),
          body: {
            "emailId": widget.email,
          });
      print(response.body);
      Fluttertoast.showToast(
          msg: "OTP has been sent Successfully",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    doOnThisLaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              SizedBox(
                height: 40.0,
              ),
              Container(
                  alignment: Alignment.center,
                  child: getheme == "Black"
                      ? Text(
                          "Enter the OTP received on your\n                 email id",
                          style: AppWidget.boldTextFeildStyle("Black"))
                      : Text(
                          "Enter the OTP received on your\n                 email id",
                          style: AppWidget.boldTextFeildStyle("White"))),
              SizedBox(
                height: 20.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                alignment: Alignment.center,
                child: Text(
                  widget.email,
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    wordSpacing: 1.0,
                    letterSpacing: 1.0,
                    color: Color(0xFFBDBDBD),
                    fontSize: 18.0,
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Form(
                key: _formKey,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: TextFormField(
                    controller: userOtp,
                    style: TextStyle(
                        color: getheme == "Black"
                            ? Colors.white54
                            : Colors.black54),
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    decoration: InputDecoration(
                      hintText: "Enter OTP",
                      hintStyle: TextStyle(
                          color: getheme == "Black"
                              ? Colors.white54
                              : Colors.black54),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              GestureDetector(
                  onTap: () {
                    verifyOtp();
                  },
                  child: getheme == "Black"
                      ? AppWidget.createAppButton("LOGIN", "Black")
                      : AppWidget.createAppButton("LOGIN", "White")),
              SizedBox(
                height: 50.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getheme == "Black"
                      ? Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Didn't get the OTP? ",
                            style: AppWidget.simpleTextFeildStyle("Black"),
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Didn't get the OTP? ",
                            style: AppWidget.simpleTextFeildStyle("White"),
                          ),
                        ),
                  GestureDetector(
                    onTap: () {
                      sendOtpAgain();
                    },
                    child: Text(
                      "Try again.",
                      style: TextStyle(
                          color: Color(0xFF5992AD),
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
