import 'package:flutter/material.dart';
import 'package:getbeyond/screens/login/bottomnav.dart';
import 'package:getbeyond/screens/login/email.dart';
import 'package:getbeyond/screens/login/focus_screens/add_link.dart';
import 'package:getbeyond/screens/login/focus_screens/home.dart';
import 'package:getbeyond/service/shared_pref.dart';

class Fine extends StatefulWidget {
  Fine({Key? key}) : super(key: key);

  @override
  State<Fine> createState() => _FineState();
}

class _FineState extends State<Fine> {
  final ValueNotifier<ThemeMode> _notifier = ValueNotifier(ThemeMode.light);

  String? value;
  final listtem = [
    "Black",
    "White",
  ];
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ThemeMode>(
      valueListenable: _notifier,
      builder: (_, mode, __) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData.light(),
          darkTheme: ThemeData.dark(),
          themeMode: mode, // Decides which theme to show, light or dark.
          home: Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        border: Border.all(color: Colors.black, width: 2)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: value,
                        hint: Text(
                          "Please Select the Theme",
                          style: TextStyle(color: Colors.black),
                        ),
                        iconSize: 36,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.black,
                        ),
                        isExpanded: true,
                        items: listtem.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          SharedPreferenceHelper().saveUserId(value!);
                          this.value = value;
                        }),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Email()));
                  },
                  child: Text(
                    "Next",
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
      );
}
