import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getheme == "Black"
                    ? SizedBox(
                        height: 20.0,
                      )
                    : SizedBox(),
                getheme == "Black"
                    ? AppWidget.createAppLogo("Black")
                    : AppWidget.createAppLogo("White"),
                getheme == "Black"
                    ? SizedBox(
                        height: 50.0,
                      )
                    : SizedBox(
                        height: 10.0,
                      ),
                getheme == "Black"
                    ? Container(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Hi Paul,",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        ),
                      )
                    : Container(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Hi Paul,",
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                      ),
                SizedBox(
                  height: 20.0,
                ),
                getheme == "Black"
                    ? Text(
                        "You have a relaxed day with 0 \nmeetings so far. What would you like \nto do next?",
                        style: AppWidget.simpleTextFeildStyle("Black"),
                      )
                    : Text(
                        "You have a relaxed day with 0 \nmeetings so far. What would you like \nto do next?",
                        style: AppWidget.simpleTextFeildStyle("White"),
                      ),
                SizedBox(
                  height: 20.0,
                ),
                getheme == "Black"
                    ? AppWidget.createSearchBar("Black")
                    : AppWidget.createSearchBar("White"),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "FOCUS",
                  style: AppWidget.lightTextFeildStyle(),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    getheme == "Black"
                        ? AppWidget.createFocusCard("Today", "Black")
                        : AppWidget.createFocusCard("Today", "White"),
                    SizedBox(
                      width: 30.0,
                    ),
                    getheme == "Black"
                        ? AppWidget.createFocusCard("This Week", "Black")
                        : AppWidget.createFocusCard("This Week", "White")
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "OTHERS RECOMMENDATIONS",
                  style: AppWidget.lightTextFeildStyle(),
                ),
                SizedBox(
                  height: 20.0,
                ),
                getheme == "Black"
                    ? AppWidget.createRecommendationBar(
                        "Add Voice Note",
                        Icon(
                          Icons.keyboard_voice_rounded,
                          color: Color(0xFF5992AD),
                          size: 30.0,
                        ),
                        0,
                        true,
                        "Black")
                    : AppWidget.createRecommendationBar(
                        "Add Voice Note",
                        Icon(
                          Icons.keyboard_voice_rounded,
                          color: Color(0xFF5992AD),
                          size: 30.0,
                        ),
                        0,
                        true,
                        "White"),
                SizedBox(
                  height: 10.0,
                ),
                getheme == "Black"
                    ? AppWidget.createRecommendationBar(
                        "Travel Recommendations",
                        Icon(Icons.mic),
                        0,
                        false,
                        "Black")
                    : AppWidget.createRecommendationBar(
                        "Travel Recommendations",
                        Icon(Icons.mic),
                        0,
                        false,
                        "White"),
                SizedBox(
                  height: 10.0,
                ),
                getheme == "Black"
                    ? AppWidget.createRecommendationBar(
                        "What's New", Icon(Icons.mic), 0, false, "Black")
                    : AppWidget.createRecommendationBar(
                        "What's New", Icon(Icons.mic), 0, false, "White"),
                SizedBox(
                  height: 10.0,
                ),
                getheme == "Black"
                    ? AppWidget.createRecommendationBar(
                        "Reconnect Recommendations",
                        Icon(Icons.mic),
                        0,
                        false,
                        "Black")
                    : AppWidget.createRecommendationBar(
                        "Reconnect Recommendations",
                        Icon(Icons.mic),
                        0,
                        false,
                        "White"),
              ],
            )),
      ),
    );
  }
}
