import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class SetUp extends StatefulWidget {
  SetUp({Key? key}) : super(key: key);

  @override
  State<SetUp> createState() => _SetUpState();
}

class _SetUpState extends State<SetUp> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getheme == "Black"
                  ? SizedBox(
                      height: 20.0,
                    )
                  : SizedBox(),
              getheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              getheme == "Black"
                  ? SizedBox(
                      height: 20.0,
                    )
                  : SizedBox(),
              getheme == "Black"
                  ? AppWidget.createSearchBar("Black")
                  : AppWidget.createSearchBar("White"),
              getheme == "Black"
                  ? SizedBox(
                      height: 30.0,
                    )
                  : SizedBox(
                      height: 10.0,
                    ),
              Row(
                children: [
                  getheme == "Black"
                      ? Text(
                          'FOCUS|',
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          'FOCUS|',
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                  getheme == "Black"
                      ? Text(
                          ' Today : 31 JAN 2022',
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          ' Today : 31 JAN 2022',
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                  getheme == "Black"
                      ? Text(
                          ' | Week|',
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          ' | Week|',
                          style: AppWidget.simpleTextFeildStyle("White"),
                        )
                ],
              ),

              // Text(
              //   "  Week | Next Month",
              //   style: AppWidget.simpleTextFeildStyle(),
              // ),
              getheme == "Black"
                  ? Text(
                      'Next Week | Next Month',
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      'Next Week | Next Month',
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? Text(
                      "MEETINGS TODAY",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "MEETINGS TODAY",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Text(
                "Set up Google/ Outlook access to view meetings and contact intelligence",
                style: AppWidget.lightTextFeildStyle(),
              ),
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? AppWidget.createSocialMediaButton(
                      "images/google.png", "Google", "Black")
                  : AppWidget.createSocialMediaButton(
                      "images/google.png", "Google", "White"),
              SizedBox(
                height: 50.0,
              ),
              getheme == "Black"
                  ? Text(
                      "REMINDERS",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "REMINDERS",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Text(
                "Set up reminders for your contacts to get reminded on their important days.",
                style: AppWidget.lightTextFeildStyle(),
              ),
              SizedBox(
                height: 40.0,
              ),
              getheme == "Black"
                  ? Text(
                      "TODAY PENDING FOLLOWUPS",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "TODAY PENDING FOLLOWUPS",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Text(
                "No pending follow ups for today",
                style: AppWidget.lightTextFeildStyle(),
              ),
              SizedBox(
                height: 40.0,
              ),
              getheme == "Black"
                  ? Text(
                      "IMPORTANT CONTACT UPDATES",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "IMPORTANT CONTACT UPDATES",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Text(
                "Grant Twitter access to view important updates from your contacts",
                style: AppWidget.lightTextFeildStyle(),
              ),
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? AppWidget.createSocialMediaButton(
                      "images/twitter.png", "Twitter", "Black")
                  : AppWidget.createSocialMediaButton(
                      "images/twitter.png", "Twitter", "White"),
            ],
          ),
        ),
      ),
    );
  }
}
