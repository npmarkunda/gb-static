import 'package:flutter/material.dart';
import 'package:getbeyond/model/travel_model.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/travel_support.dart';
import 'package:getbeyond/widget/widget_support.dart';

class TravelRecommendation extends StatefulWidget {
  TravelRecommendation({Key? key}) : super(key: key);

  @override
  State<TravelRecommendation> createState() => _TravelRecommendationState();
}

class _TravelRecommendationState extends State<TravelRecommendation> {
  List<TravelModel> categories = [];

  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    categories = getTravelRecommendations();
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getheme == "Black"
                  ? SizedBox(
                      height: 30.0,
                    )
                  : SizedBox(),
              getheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              getheme == "Black"
                  ? SizedBox(
                      height: 20.0,
                    )
                  : SizedBox(),
              getheme == "Black"
                  ? AppWidget.createSearchBar("Black")
                  : AppWidget.createSearchBar("White"),
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? Text(
                      'TRAVEL RECOMMENDATIONS',
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      'TRAVEL RECOMMENDATIONS',
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              SizedBox(
                height: 30.0,
              ),
              getheme == "Black"
                  ? Text(
                      'LONDON - 12 Feb - 17 Feb',
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      'LONDON - 12 Feb - 17 Feb',
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Container(
                height: 170,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: categories.length,
                    itemBuilder: (context, index) {
                      return CategoryCard(
                        image: categories[index].image,
                        name: categories[index].name,
                        company: categories[index].company,
                        icon: categories[index].icon,
                        time: categories[index].time,
                        clock: true,
                      );
                    }),
              ),
              getheme == "Black"
                  ? Text(
                      'SAN FRANCISCO - 26 Feb - 07 MAR',
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      'SAN FRANCISCO - 26 Feb - 07 MAR',
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Container(
                height: 170,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: categories.length,
                    itemBuilder: (context, index) {
                      return CategoryCard(
                        image: categories[index].image,
                        name: categories[index].name,
                        company: categories[index].company,
                        icon: categories[index].icon,
                        time: categories[index].time,
                        clock: true,
                        theme: getheme == "Black" ? "Black" : "White",
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CategoryCard extends StatelessWidget {
  final String? image, name, company, time, theme;
  bool clock;
  Icon icon;

  CategoryCard(
      {required this.image,
      required this.name,
      required this.company,
      required this.icon,
      this.time,
      required this.clock,
      this.theme});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              height: 55,
              width: 55,
              child: Image.asset(
                image!,
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              theme == "Black"
                  ? Text(
                      name!,
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      name!,
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Text(
                company!,
                style: AppWidget.lightTextFeildStyle(),
              ),
            ],
          ),
          clock
              ? SizedBox(
                  width: 10.0,
                )
              : SizedBox(
                  width: 40.0,
                ),
          clock
              ? theme == "Black"
                  ? Text(
                      time!,
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      time!,
                      style: AppWidget.simpleTextFeildStyle("White"),
                    )
              : Container(),
          Container(
            child: icon,
          )
        ],
      ),
    );
  }
}
