import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class AddLink extends StatefulWidget {
  AddLink({Key? key}) : super(key: key);

  @override
  State<AddLink> createState() => _AddLinkState();
}

class _AddLinkState extends State<AddLink> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            getheme == "Black"
                ? SizedBox(
                    height: 20.0,
                  )
                : SizedBox(),
            getheme == "Black"
                ? AppWidget.createAppLogo("Black")
                : AppWidget.createAppLogo("White"),
            getheme == "Black"
                ? SizedBox(
                    height: 50.0,
                  )
                : SizedBox(
                    height: 10.0,
                  ),
            getheme == "Black"
                ? Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Hi Paul,",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    ),
                  )
                : Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Hi Paul,",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
                  ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              alignment: Alignment.topLeft,
              child: Text(
                "We couldn't find your contacts and interactions. Please link your email/calender to get beyond.",
                style: AppWidget.lightTextFeildStyle(),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              alignment: Alignment.topLeft,
              child: Text(
                "if your contacts and interactions are not syncing, please email us at hello@getbeyond.ai",
                style: AppWidget.lightTextFeildStyle(),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            getheme == "Black"
                ? AppWidget.createSocialMediaButton(
                    'images/google.png', "Google", "Black")
                : AppWidget.createSocialMediaButton(
                    'images/google.png', "Google", "White"),
            SizedBox(
              height: 10.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: [
                Text(
                  "Read our human readable",
                  style: AppWidget.lightTextFeildStyle(),
                ),
                getheme == "Black"
                    ? Text(" privacy policy",
                        style: AppWidget.boldTextwithColorFeildStyle("Black"))
                    : Text(" privacy policy",
                        style: AppWidget.boldTextwithColorFeildStyle("White"))
              ],
            ),
            Text(
              "to understand how your data is used",
              style: AppWidget.lightTextFeildStyle(),
            )
          ],
        ),
      ),
    );
  }
}
