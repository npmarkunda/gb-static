import 'package:flutter/material.dart';
import 'package:getbeyond/model/contactupdates.dart';
import 'package:getbeyond/model/followups.dart';
import 'package:getbeyond/model/meeting_today.dart';
import 'package:getbeyond/model/reminders.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/contact_updates.dart';
import 'package:getbeyond/widget/meeting_widget.dart';
import 'package:getbeyond/widget/pending_fllowups.dart';
import 'package:getbeyond/widget/reminders_widget.dart';
import 'package:getbeyond/widget/widget_support.dart';

class TodayMeeting extends StatefulWidget {
  TodayMeeting({Key? key}) : super(key: key);

  @override
  State<TodayMeeting> createState() => _TodayMeetingState();
}

class _TodayMeetingState extends State<TodayMeeting> {
  List<MeetingToday> categories = [];
  List<Reminders> reminders = [];
  List<FollowUps> follow = [];
  List<ContactUpdates> updates = [];
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    doonthislaunch();
    super.initState();
    reminders = getReminders();
    categories = getCategories();
    follow = getFollowups();
    updates = getUpdates();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getheme == "Black"
                  ? SizedBox(
                      height: 20.0,
                    )
                  : SizedBox(),
              getheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              getheme == "Black"
                  ? SizedBox(
                      height: 20.0,
                    )
                  : SizedBox(),
              getheme == "Black"
                  ? AppWidget.createSearchBar("Black")
                  : AppWidget.createSearchBar("White"),
              getheme == "Black"
                  ? SizedBox(
                      height: 30.0,
                    )
                  : SizedBox(
                      height: 10.0,
                    ),
              Row(
                children: [
                  getheme == "Black"
                      ? Text(
                          "FOCUS|",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          "FOCUS|",
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                  getheme == "Black"
                      ? Text(
                          " Today : 31 JAN 2022",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          " Today : 31 JAN 2022",
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                  getheme == "Black"
                      ? Text(
                          " | Week|",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          " | Week|",
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                ],
              ),
              Text(
                "Next Week | Next Month",
                style: AppWidget.lightTextFeildStyle(),
              ),
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? Text(
                      "MEETINGS TODAY",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "MEETINGS TODAY",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Container(
                height: 170,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: categories.length,
                    itemBuilder: (context, index) {
                      return CategoryCard(
                        image: categories[index].image,
                        name: categories[index].name,
                        company: categories[index].company,
                        icon: categories[index].icon,
                        time: categories[index].time,
                        clock: true,
                      );
                    }),
              ),
              SizedBox(
                height: 10.0,
              ),
              getheme == "Black"
                  ? Text(
                      "REMINDERS",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "REMINDERS",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Container(
                height: 80,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: reminders.length,
                    itemBuilder: (context, index) {
                      return CategoryCard(
                        image: reminders[index].image,
                        name: reminders[index].name,
                        company: reminders[index].company,
                        icon: reminders[index].icon,
                        clock: false,
                        theme: getheme == "Black" ? "Black" : "White",
                      );
                    }),
              ),
              SizedBox(
                height: 10.0,
              ),
              getheme == "Black"
                  ? Text(
                      "TODAY PENDING FOLLOWUPS",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "TODAY PENDING FOLLOWUPS",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Container(
                height: 80,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: reminders.length,
                    itemBuilder: (context, index) {
                      return FollowupCard(
                        image: follow[index].image,
                        detail: follow[index].detail,
                        icon: follow[index].icon,
                        theme: getheme == "Black" ? "Black" : "White",
                      );
                    }),
              ),
              SizedBox(
                height: 30.0,
              ),
              getheme == "Black"
                  ? Text(
                      "IMPORTANT CONTACT UPDATES",
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      "IMPORTANT CONTACT UPDATES",
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Divider(
                thickness: 2.0,
              ),
              Container(
                height: 80,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: reminders.length,
                    itemBuilder: (context, index) {
                      return ContactCard(
                        image: updates[index].image,
                        detail: updates[index].detail,
                        id: updates[index].id,
                        tag: updates[index].tag,
                        image1: updates[index].image1,
                        theme: getheme == "Black" ? "Black" : "White",
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CategoryCard extends StatelessWidget {
  final String? image, name, company, time, theme;
  bool clock;
  Icon icon;

  CategoryCard(
      {required this.image,
      required this.name,
      required this.company,
      required this.icon,
      this.time,
      required this.clock,
      this.theme});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              height: 55,
              width: 55,
              child: Image.asset(
                image!,
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              theme == "Black"
                  ? Text(
                      name!,
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      name!,
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Text(
                company!,
                style: AppWidget.lightTextFeildStyle(),
              )
            ],
          ),
          clock
              ? SizedBox(
                  width: 20.0,
                )
              : SizedBox(
                  width: 70.0,
                ),
          clock
              ? theme == "Black"
                  ? Text(
                      time!,
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      time!,
                      style: AppWidget.simpleTextFeildStyle("White"),
                    )
              : Container(),
          SizedBox(
            width: 10.0,
          ),
          icon
        ],
      ),
    );
  }
}

class FollowupCard extends StatelessWidget {
  final String? image, detail, theme;
  Icon icon;

  FollowupCard({
    required this.image,
    this.theme,
    required this.icon,
    this.detail,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              height: 55,
              width: 55,
              child: Image.asset(
                image!,
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              theme == "Black"
                  ? Text(
                      detail!,
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      detail!,
                      style: AppWidget.simpleTextFeildStyle("White"),
                    )
            ],
          ),
          SizedBox(
            width: 10.0,
          ),
          icon
        ],
      ),
    );
  }
}

class ContactCard extends StatelessWidget {
  final String? image, tag, detail, id, image1, theme;

  ContactCard(
      {required this.image,
      this.image1,
      this.detail,
      this.id,
      this.tag,
      this.theme});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              height: 55,
              width: 55,
              child: Image.asset(
                image!,
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              theme == "Black"
                  ? Text(
                      detail!,
                      style: AppWidget.simpleTextFeildStyle("Black"),
                    )
                  : Text(
                      detail!,
                      style: AppWidget.simpleTextFeildStyle("White"),
                    ),
              Row(
                children: [
                  theme == "Black"
                      ? Text(
                          tag! + ",",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          tag! + ",",
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                  theme == "Black"
                      ? Text(
                          id!,
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      : Text(
                          id!,
                          style: AppWidget.simpleTextFeildStyle("White"),
                        ),
                ],
              )
            ],
          ),
          SizedBox(
            width: 10.0,
          ),
          Image.asset(
            image1!,
            height: 28,
            width: 28,
          )
        ],
      ),
    );
  }
}
