import 'package:flutter/material.dart';
import 'package:getbeyond/screens/login/otp.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Username extends StatefulWidget {
  String name, email;
  Username({required this.name, required this.email});

  @override
  State<Username> createState() => _UsernameState();
}

class _UsernameState extends State<Username> {
  TextEditingController usernameeditingcontroller = new TextEditingController();
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          children: [
            SizedBox(
              height: 30.0,
            ),
            getheme == "Black"
                ? AppWidget.createAppLogo("Black")
                : AppWidget.createAppLogo("White"),
            SizedBox(
              height: 60.0,
            ),
            getheme == "Black"
                ? Container(
                    alignment: Alignment.center,
                    child: Text("CONGRATULATIONS !!",
                        style: AppWidget.boldTextFeildStyle("Black")))
                : Container(
                    alignment: Alignment.center,
                    child: Text("CONGRATULATIONS !!",
                        style: AppWidget.boldTextFeildStyle("White"))),
            SizedBox(
              height: 10.0,
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 6.0),
                alignment: Alignment.center,
                child: getheme == "Black"
                    ? Text("Get beyond with your unique handle",
                        style: AppWidget.simpleTextFeildStyle("Black"))
                    : Text("Get beyond with your unique handle",
                        style: AppWidget.simpleTextFeildStyle("White"))),
            SizedBox(
              height: 40.0,
            ),
            Container(
              alignment: Alignment.center,
              child: Text("PLEASE EDIT IF YOU WISH TO CHANGE",
                  style: AppWidget.lightTextFeildStyle()),
            ),
            SizedBox(
              height: 30.0,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: TextField(
                controller: usernameeditingcontroller,
                style: TextStyle(
                    color:
                        getheme == "Black" ? Colors.white54 : Colors.black54),
                decoration: InputDecoration(
                    hintText: widget.name,
                    hintStyle: TextStyle(
                        color: getheme == "Black"
                            ? Colors.white54
                            : Colors.black54)),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                  "Only 28% people get their first choice.\n           Reserve yours now.",
                  style: AppWidget.lightTextFeildStyle()),
            ),
            SizedBox(
              height: 30.0,
            ),
            GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => OTP(email: widget.email)));
                },
                child: getheme == "Black"
                    ? AppWidget.createAppButton("GET BEYOND", "Black")
                    : AppWidget.createAppButton("GET BEYOND", "White")),
            SizedBox(
              height: 50.0,
            ),
          ],
        ),
      ),
    );
  }
}
