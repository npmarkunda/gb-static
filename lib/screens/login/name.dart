import 'package:flutter/material.dart';
import 'package:getbeyond/screens/login/username.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Name extends StatefulWidget {
  String email;
  Name({required this.email});

  @override
  State<Name> createState() => _NameState();
}

class _NameState extends State<Name> {
  TextEditingController usernameeditingController = new TextEditingController();
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              SizedBox(
                height: 30.0,
              ),
              getheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              SizedBox(
                height: 60.0,
              ),
              getheme == "Black"
                  ? Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Looks like you are new here",
                        style: AppWidget.boldTextFeildStyle("Black"),
                      ),
                    )
                  : Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Looks like you are new here",
                        style: AppWidget.boldTextFeildStyle("White"),
                      ),
                    ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                alignment: Alignment.center,
                child: getheme == "Black"
                    ? Text(
                        "Let's get you beyond",
                        style: AppWidget.simpleTextFeildStyle("Black"),
                      )
                    : Text(
                        "Let's get you beyond",
                        style: AppWidget.simpleTextFeildStyle("White"),
                      ),
              ),
              SizedBox(
                height: 10.0,
              ),
              SizedBox(
                height: 40.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                alignment: Alignment.center,
                child: Text("MY FREIND CALL ME",
                    style: AppWidget.lightTextFeildStyle()),
              ),
              SizedBox(
                height: 40.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                child: TextField(
                  controller: usernameeditingController,
                  style: TextStyle(
                      color:
                          getheme == "Black" ? Colors.white54 : Colors.black54),
                  decoration: InputDecoration(
                      hintText: "Name",
                      hintStyle: TextStyle(
                          color: getheme == "Black"
                              ? Colors.white54
                              : Colors.black54)),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Username(
                                  name: usernameeditingController.text,
                                  email: widget.email,
                                )));
                  },
                  child: getheme == "Black"
                      ? AppWidget.createAppButton("NEXT", "Black")
                      : AppWidget.createAppButton("NEXT", "White")),
              SizedBox(
                height: 50.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
