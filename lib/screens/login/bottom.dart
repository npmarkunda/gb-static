import 'package:flutter/material.dart';
import 'package:getbeyond/pageview.dart';
import 'package:getbeyond/screens/login/bookmark.dart';
import 'package:getbeyond/screens/login/contact_screen/connection.dart';
import 'package:getbeyond/screens/login/contact_screen/engagement.dart';
import 'package:getbeyond/screens/login/contact_screen/group.dart';
import 'package:getbeyond/screens/login/contact_screen/mic.dart';
import 'package:getbeyond/screens/login/contact_screen/notification.dart';
import 'package:getbeyond/screens/login/search/search_screen.dart';
import 'package:getbeyond/screens/login/settings.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Bottom extends StatefulWidget {
  Bottom({Key? key}) : super(key: key);

  @override
  State<Bottom> createState() => _BottomState();
}

class _BottomState extends State<Bottom> {
  int currentTabIndex = 0;
  late List<Widget> pages;

  late Widget currentPage;

  late Connection connection;
  late Engagement engagement;

  late Group group;

  late Mictake mic;
  late Notificationtab notification;
  Color active = Color(0xFF5992AD);

  @override
  void initState() {
    super.initState();

    connection = Connection();
    group = Group();
    engagement = Engagement();

    mic = Mictake();
    notification = Notificationtab();
    pages = [engagement, connection, mic, group, notification];
    doonthislaunch();
    currentPage = engagement;
  }

  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20.0,
            ),
            AppWidget.createAppLogo("Black"),
            SizedBox(
              height: 20.0,
            ),
            AppWidget.getYourProfile("Black"),
            Divider(
              color: Colors.white,
            ),
            BottomNavigationBar(
              backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              onTap: (int index) {
                setState(() {
                  currentTabIndex = index;
                  currentPage = pages[index];
                });
              },
              currentIndex: currentTabIndex,
              type: BottomNavigationBarType.fixed,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                    icon: Icon(
                      Icons.flash_on_outlined,
                      size: 30.0,
                      color: getheme == "Black"
                          ? Color(0xFF999999)
                          : Color(0xFF5992AD),
                    ),
                    label: "Home",
                    activeIcon: Icon(
                      Icons.flash_on_outlined,
                      size: 30.0,
                      color: getheme == "Black"
                          ? Color(0xFF19A1E5)
                          : Color(0xFF5992AD),
                    )),
                BottomNavigationBarItem(
                    label: "Home",
                    icon: Icon(
                      Icons.link,
                      size: 30.0,
                      color: getheme == "Black"
                          ? Color(0xFF999999)
                          : Color(0xFF5992AD),
                    ),
                    activeIcon: Icon(
                      Icons.link,
                      size: 30.0,
                      color: getheme == "Black"
                          ? Color(0xFF19A1E5)
                          : Color(0xFF5992AD),
                    )),
                BottomNavigationBarItem(
                    label: "Home",
                    icon: Icon(
                      Icons.mic,
                      size: 30.0,
                      color: getheme == "Black"
                          ? Color(0xFF999999)
                          : Color(0xFF5992AD),
                    ),
                    activeIcon: Icon(
                      Icons.mic,
                      size: 30.0,
                      color: getheme == "Black"
                          ? Color(0xFF19A1E5)
                          : Color(0xFF5992AD),
                    )),
                BottomNavigationBarItem(
                  label: "Home",
                  icon: Icon(
                    Icons.loop,
                    size: 30.0,
                    color: getheme == "Black"
                        ? Color(0xFF999999)
                        : Color(0xFF5992AD),
                  ),
                  activeIcon: Icon(
                    Icons.loop,
                    size: 30.0,
                    color: getheme == "Black"
                        ? Color(0xFF19A1E5)
                        : Color(0xFF5992AD),
                  ),
                ),
                BottomNavigationBarItem(
                  label: "Home",
                  icon: Icon(
                    Icons.notifications_none_rounded,
                    size: 30.0,
                    color: getheme == "Black"
                        ? Color(0xFF999999)
                        : Color(0xFF5992AD),
                  ),
                  activeIcon: Icon(
                    Icons.notifications_none_rounded,
                    size: 30.0,
                    color: getheme == "Black"
                        ? Color(0xFF19A1E5)
                        : Color(0xFF5992AD),
                  ),
                )
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 410,
              child: currentPage,
            ),
          ],
        ),
      ),
    );
  }
}
