import 'package:flutter/material.dart';
import 'package:getbeyond/pageview.dart';
import 'package:getbeyond/screens/login/bookmark.dart';
import 'package:getbeyond/screens/login/search/search_screen.dart';
import 'package:getbeyond/screens/login/settings.dart';
import 'package:getbeyond/service/shared_pref.dart';

class Bottomnav extends StatefulWidget {
  Bottomnav({Key? key}) : super(key: key);

  @override
  State<Bottomnav> createState() => _BottomnavState();
}

class _BottomnavState extends State<Bottomnav> {
  int currentTabIndex = 0;
  late List<Widget> pages;

  late Widget currentPage;

  late PageSystem page;

  late Settings settings;
  late Searchscreen search;
  late Bookmark bookmark;
  Color active = Color(0xFF5992AD);

  @override
  void initState() {
    super.initState();

    page = PageSystem();

    settings = Settings();
    search = Searchscreen();
    bookmark = Bookmark();
    pages = [page, search, bookmark, settings];
    doonthislaunch();
    currentPage = page;
  }

  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        onTap: (int index) {
          setState(() {
            currentTabIndex = index;
            currentPage = pages[index];
          });
        },
        currentIndex: currentTabIndex,
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home_outlined,
                size: 30.0,
                color:
                    getheme == "Black" ? Color(0xFF999999) : Color(0xFF5992AD),
              ),
              label: "Home",
              activeIcon: Icon(
                Icons.home_outlined,
                size: 30.0,
                color:
                    getheme == "Black" ? Color(0xFF19A1E5) : Color(0xFF5992AD),
              )),
          BottomNavigationBarItem(
              label: "Home",
              icon: Icon(
                Icons.search,
                size: 30.0,
                color:
                    getheme == "Black" ? Color(0xFF999999) : Color(0xFF5992AD),
              ),
              activeIcon: Icon(
                Icons.search,
                size: 30.0,
                color:
                    getheme == "Black" ? Color(0xFF19A1E5) : Color(0xFF5992AD),
              )),
          BottomNavigationBarItem(
              label: "Home",
              icon: Icon(
                Icons.bookmark_border_outlined,
                size: 30.0,
                color:
                    getheme == "Black" ? Color(0xFF999999) : Color(0xFF5992AD),
              ),
              activeIcon: Icon(
                Icons.bookmark_border_outlined,
                size: 30.0,
                color:
                    getheme == "Black" ? Color(0xFF19A1E5) : Color(0xFF5992AD),
              )),
          BottomNavigationBarItem(
            label: "Home",
            icon: Icon(
              Icons.settings,
              size: 30.0,
              color: getheme == "Black" ? Color(0xFF999999) : Color(0xFF5992AD),
            ),
            activeIcon: Icon(
              Icons.settings,
              size: 30.0,
              color: getheme == "Black" ? Color(0xFF19A1E5) : Color(0xFF5992AD),
            ),
          )
        ],
      ),
      body: currentPage,
    );
  }
}
