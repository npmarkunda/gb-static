import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:getbeyond/screens/login/Register.dart';
import 'package:getbeyond/screens/login/name.dart';
import 'package:getbeyond/screens/login/otp.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';
import 'package:http/http.dart' as http;

class Email extends StatefulWidget {
  Email({Key? key}) : super(key: key);

  @override
  State<Email> createState() => _EmailState();
}

class _EmailState extends State<Email> {
  TextEditingController useremaileditingcontroller =
      new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late String getTheme, email, message;
  getTheTheme() async {
    String? getTheme = await SharedPreferenceHelper().getUserId()!;
    setState(() {});
  }

  doOnThisLaunch() async {
    await getTheTheme();
    setState(() {});
  }

  @override
  void initState() {
    doOnThisLaunch();
    super.initState();
  }

  userLogin() async {
    try {
      var response = await http.post(
          Uri.parse(
            "https://api-dev.getbeyond.ai/user/login",
          ),
          body: ({
            "emailId": email,
          }));
      print(response.body);
      Map<String, dynamic> jsonData = jsonDecode(response.body);
      message = jsonData["msg"];
      if (message == "Error in getting user details") {
        Fluttertoast.showToast(
            msg: "No User Found for that Email",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => OTP(
                      email: email,
                    )));
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getTheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              SizedBox(
                height: 30.0,
              ),
              getTheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              SizedBox(
                height: 60.0,
              ),
              getTheme == "Black"
                  ? Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Enter your email id to signin",
                        style: AppWidget.boldTextFeildStyle("Black"),
                      ),
                    )
                  : Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Enter your email id to signin",
                        style: AppWidget.boldTextFeildStyle("White"),
                      ),
                    ),
              SizedBox(
                height: 20.0,
              ),
              getTheme == "Black"
                  ? Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      alignment: Alignment.center,
                      child: Text(
                        "We will send you a OTP (One-time-\n       password) on this email id.",
                        style: AppWidget.simpleTextFeildStyle("Black"),
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      alignment: Alignment.center,
                      child: Text(
                        "We will send you a OTP (One-time-\n       password) on this email id.",
                        style: AppWidget.simpleTextFeildStyle("White"),
                      ),
                    ),
              SizedBox(
                height: 30.0,
              ),
              Form(
                key: _formKey,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: TextFormField(
                    controller: useremaileditingcontroller,
                    style: TextStyle(
                        color: getTheme == "Black"
                            ? Colors.white54
                            : Colors.black54),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Email';
                      } else if (!value.contains('@')) {
                        return 'Please Enter Valid Email';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        hintText: "Email",
                        hintStyle: TextStyle(
                            color: getTheme == "Black"
                                ? Colors.white54
                                : Colors.black54)),
                  ),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              getTheme == "Black"
                  ? GestureDetector(
                      onTap: () {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            email = useremaileditingcontroller.text;
                          });
                          userLogin();
                        }
                      },
                      child: AppWidget.createAppButton("GET BEYOND", "Black"))
                  : GestureDetector(
                      onTap: () {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            email = useremaileditingcontroller.text;
                          });
                          userLogin();
                        }
                      },
                      child: AppWidget.createAppButton("GET BEYOND", "White")),
              SizedBox(
                height: 50.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getTheme == "Black"
                      ? Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Don't have an account?",
                            style: AppWidget.simpleTextFeildStyle("Black"),
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Don't have an account?",
                            style: AppWidget.simpleTextFeildStyle("White"),
                          ),
                        ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Register()));
                    },
                    child: Text(
                      "Sign Up",
                      style: TextStyle(
                          color: Color(0xFF5992AD),
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
