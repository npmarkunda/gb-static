import 'package:flutter/material.dart';
import 'package:getbeyond/model/search_model.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/search_widget.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Searchscreen extends StatefulWidget {
  Searchscreen({Key? key}) : super(key: key);

  @override
  State<Searchscreen> createState() => _SearchscreenState();
}

class _SearchscreenState extends State<Searchscreen> {
  List<SearchModel> categories = [];
  bool issearching = false;
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    categories = getSearches();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          child: Column(children: [
            SizedBox(
              height: 20.0,
            ),
            AppWidget.createAppLogo("Black"),
            SizedBox(
              height: 20.0,
            ),
            getheme == "Black"
                ? Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Color(0XFF1F2023),
                        borderRadius: BorderRadius.circular(10)),
                    child: TextField(
                      onChanged: (value) {
                        issearching = true;
                        setState(() {});
                      },
                      decoration: InputDecoration(
                          hintText: 'Search Contacts',
                          border: InputBorder.none,
                          hintStyle: AppWidget.boldTextFeildStyle("Black"),
                          suffixIcon: Icon(
                            Icons.search,
                            size: 30.0,
                            color: Color(0xFF19A1E5),
                          )),
                    ),
                  )
                : Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Color(0XFFF3F7F9),
                        borderRadius: BorderRadius.circular(10)),
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: 'Search Contacts',
                          border: InputBorder.none,
                          hintStyle: AppWidget.boldTextFeildStyle("White"),
                          suffixIcon: Icon(
                            Icons.search,
                            size: 30.0,
                            color: Color(0xFF5992AD),
                          )),
                    ),
                  ),
            SizedBox(
              height: 10.0,
            ),
            issearching
                ? Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "4 Matches found",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        ),
                        Container(
                          height: 170,
                          child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: categories.length,
                              itemBuilder: (context, index) {
                                return CategoryCard(
                                  image: categories[index].image,
                                  name: categories[index].name,
                                  company: categories[index].company,
                                  detail: categories[index].detail,
                                );
                              }),
                        ),
                      ],
                    ),
                  )
                : Container(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "ALL ",
                              style: AppWidget.boldTextwithColorFeildStyle(
                                  "Black"),
                            ),
                            Text(
                              " | FAVOURITE",
                              style: AppWidget.boldTextFeildStyle("Black"),
                            )
                          ],
                        ),
                        Container(
                          height: 170,
                          child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: categories.length,
                              itemBuilder: (context, index) {
                                return CategoryCard(
                                  image: categories[index].image,
                                  name: categories[index].name,
                                  company: categories[index].company,
                                  detail: categories[index].detail,
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
          ]),
        ));
  }
}

class CategoryCard extends StatelessWidget {
  final String? image, name, company, theme, detail;

  CategoryCard(
      {required this.image,
      required this.name,
      required this.company,
      this.theme,
      this.detail});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Container(
              height: 55,
              width: 55,
              child: Image.asset(
                image!,
                fit: BoxFit.fill,
              ),
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name!,
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              Text(
                company!,
                style: AppWidget.lightTextFeildStyle(),
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                children: [
                  Text(
                    detail!,
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            width: 120.0,
          ),
          Icon(
            Icons.star,
            color: Colors.blue,
            size: 30.0,
          )
        ],
      ),
    );
  }
}
