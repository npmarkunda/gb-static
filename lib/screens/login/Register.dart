import 'package:flutter/material.dart';
import 'package:getbeyond/screens/login/email.dart';
import 'package:getbeyond/screens/login/name.dart';
import 'package:getbeyond/screens/login/otp.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:http/http.dart' as http;
import 'package:getbeyond/widget/widget_support.dart';

class Register extends StatefulWidget {
  Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  TextEditingController useremaileditingcontroller =
      new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  late String getheme, email;
  getTheTheme() async {
    getheme = (await SharedPreferenceHelper().getUserId())!;
    setState(() {});
  }

  doOnThisLaunch() async {
    await getTheTheme();
    setState(() {});
  }

  @override
  void initState() {
    doOnThisLaunch();
    super.initState();
  }

  userRegister() async {
    try {
      var response = await http.post(
          Uri.parse(
            "https://api-dev.getbeyond.ai/user/register",
          ),
          body: ({
            "emailId": email,
          }));
      print(response.body);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => Name(
                    email: email,
                  )));
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              SizedBox(
                height: 30.0,
              ),
              getheme == "Black"
                  ? AppWidget.createAppLogo("Black")
                  : AppWidget.createAppLogo("White"),
              SizedBox(
                height: 60.0,
              ),
              getheme == "Black"
                  ? Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Enter your email id to signin",
                        style: AppWidget.boldTextFeildStyle("Black"),
                      ),
                    )
                  : Container(
                      alignment: Alignment.center,
                      child: Text(
                        "Enter your email id to Signup",
                        style: AppWidget.boldTextFeildStyle("White"),
                      ),
                    ),
              SizedBox(
                height: 20.0,
              ),
              getheme == "Black"
                  ? Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      alignment: Alignment.center,
                      child: Text(
                        "We will send you a OTP (One-time-\n       password) on this email id.",
                        style: AppWidget.simpleTextFeildStyle("Black"),
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      alignment: Alignment.center,
                      child: Text(
                        "We will send you a OTP (One-time-\n       password) on this email id.",
                        style: AppWidget.simpleTextFeildStyle("White"),
                      ),
                    ),
              SizedBox(
                height: 30.0,
              ),
              Form(
                key: _formKey,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: TextFormField(
                    controller: useremaileditingcontroller,
                    style: TextStyle(
                        color: getheme == "Black"
                            ? Colors.white54
                            : Colors.black54),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Email';
                      } else if (!value.contains('@')) {
                        return 'Please Enter Valid Email';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        hintText: "Email",
                        hintStyle: TextStyle(
                            color: getheme == "Black"
                                ? Colors.white54
                                : Colors.black54)),
                  ),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              getheme == "Black"
                  ? GestureDetector(
                      onTap: () {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            email = useremaileditingcontroller.text;
                          });
                          userRegister();
                        }
                      },
                      child: AppWidget.createAppButton("GET BEYOND", "Black"))
                  : GestureDetector(
                      onTap: () {
                        if (_formKey.currentState!.validate()) {
                          setState(() {
                            email = useremaileditingcontroller.text;
                          });
                          userRegister();
                        }
                      },
                      child: AppWidget.createAppButton("GET BEYOND", "White")),
              SizedBox(
                height: 50.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getheme == "Black"
                      ? Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Already have an account?",
                            style: AppWidget.simpleTextFeildStyle("Black"),
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Already have an account?",
                            style: AppWidget.simpleTextFeildStyle("White"),
                          ),
                        ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Email()));
                    },
                    child: Text(
                      "Log In",
                      style: TextStyle(
                          color: Color(0xFF5992AD),
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
