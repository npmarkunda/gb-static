import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Mictake extends StatefulWidget {
  Mictake({Key? key}) : super(key: key);

  @override
  State<Mictake> createState() => _MictakeState();
}

class _MictakeState extends State<Mictake> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white54,
                  ),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  Text(
                    "Edit Note",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  ),
                  SizedBox(
                    width: 210.0,
                  ),
                  Icon(
                    Icons.mic,
                    color: Colors.blue,
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                  Icon(
                    Icons.add,
                    color: Colors.blue,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "NOTES",
              style: AppWidget.simpleTextFeildStyle("Black"),
            ),
            Divider(
              color: Colors.white,
            ),
            Text(
              "Kid - Sunday",
              style: AppWidget.simpleTextFeildStyle("Black"),
            ),
            Text(
              "Wife - Subhra",
              style: AppWidget.simpleTextFeildStyle("Black"),
            )
          ],
        ),
      ),
    );
  }
}
