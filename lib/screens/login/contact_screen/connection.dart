import 'package:flutter/material.dart';
import 'package:getbeyond/model/connect_model.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/connect_widget.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Connection extends StatefulWidget {
  Connection({Key? key}) : super(key: key);

  @override
  State<Connection> createState() => _ConnectionState();
}

class _ConnectionState extends State<Connection> {
  List<ConnectModel> categories = [];

  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    doonthislaunch();
    categories = getConnect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              AppWidget.createSocialMediaButton(
                  "", "Sudip is looking for", "Black"),
              Container(
                height: 350,
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: categories.length,
                    itemBuilder: (context, index) {
                      return CategoryCard(
                        image1: categories[index].image1,
                        image2: categories[index].image2,
                        heading: categories[index].heading,
                        intoducedto: categories[index].intoducedto,
                        connections: categories[index].connections,
                        theme: getheme == "Black" ? "Black" : "White",
                      );
                    }),
              ),
              AppWidget.createSocialMediaButton(
                  "", "Sudip can help with", "Black"),
            ],
          ),
        ),
      ),
    );
  }
}

class CategoryCard extends StatelessWidget {
  final String? image1, image2, heading, connections, intoducedto, theme;

  CategoryCard(
      {required this.image1,
      required this.heading,
      required this.connections,
      required this.intoducedto,
      this.image2,
      this.theme});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              heading!,
              style: AppWidget.boldTextFeildStyle("Black"),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              connections!,
              style: AppWidget.simpleTextFeildStyle("Black"),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: [
                Container(
                  height: 50,
                  width: 50,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Image.asset(
                      image1!,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                SizedBox(
                  width: 20.0,
                ),
                Container(
                  height: 50,
                  width: 50,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Image.asset(
                      image2!,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            theme == "Black"
                ? Text(
                    "You introduced " + intoducedto!,
                    style: AppWidget.lightTextFeildStyle(),
                  )
                : Text(
                    "You introduced " + intoducedto!,
                    style: AppWidget.lightTextFeildStyle(),
                  ),
            // Flexible(
            //   child: Text(
            //     intoducedto!,
            //     style: AppWidget.boldTextFeildStyle("White"),
            //   ),
            // ),
          ],
        ));
  }
}
