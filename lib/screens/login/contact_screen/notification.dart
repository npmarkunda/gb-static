import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Notificationtab extends StatefulWidget {
  Notificationtab({Key? key}) : super(key: key);

  @override
  State<Notificationtab> createState() => _NotificationtabState();
}

class _NotificationtabState extends State<Notificationtab> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "SET REMINDER",
              style: AppWidget.simpleTextFeildStyle("Black"),
            ),
            Divider(
              color: Colors.white,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getheme == "Black"
                          ? Text("Tomorrow",
                              style: AppWidget.boldTextFeildStyle("Black"))
                          : Text(
                              "Tomorrow",
                              style: AppWidget.boldTextFeildStyle("White"),
                            ),
                      getheme == "Black"
                          ? Text("SUN - 09:00AM",
                              style: AppWidget.simpleTextFeildStyle("Black"))
                          : Text(
                              "SUN - 09:00AM",
                              style: AppWidget.simpleTextFeildStyle("White"),
                            )
                    ],
                  ),
                  Icon(
                    Icons.calendar_today_outlined,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getheme == "Black"
                          ? Text("Next Week",
                              style: AppWidget.boldTextFeildStyle("Black"))
                          : Text(
                              "Next Week",
                              style: AppWidget.boldTextFeildStyle("White"),
                            ),
                      getheme == "Black"
                          ? Text("SUN - 09:00AM",
                              style: AppWidget.simpleTextFeildStyle("Black"))
                          : Text(
                              "SUN - 09:00AM",
                              style: AppWidget.simpleTextFeildStyle("White"),
                            )
                    ],
                  ),
                  Icon(
                    Icons.calendar_today_outlined,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      getheme == "Black"
                          ? Text("Next Month",
                              style: AppWidget.boldTextFeildStyle("Black"))
                          : Text(
                              "Next Month",
                              style: AppWidget.boldTextFeildStyle("White"),
                            ),
                      getheme == "Black"
                          ? Text("SUN - FEB 16 - 09:00AM",
                              style: AppWidget.simpleTextFeildStyle("Black"))
                          : Text(
                              "SUN - FEB 16 - 09:00AM",
                              style: AppWidget.simpleTextFeildStyle("White"),
                            )
                    ],
                  ),
                  Icon(
                    Icons.calendar_today_outlined,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
