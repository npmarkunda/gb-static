import 'package:flutter/material.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Engagement extends StatefulWidget {
  Engagement({Key? key}) : super(key: key);

  @override
  State<Engagement> createState() => _EngagementState();
}

class _EngagementState extends State<Engagement> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "ENGAGEMENT INTELLIGENCE",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AppWidget.getYourContainer("Freindly", ""),
                  AppWidget.getYourContainer("Perfectionist", ""),
                  AppWidget.getYourContainer("Self Deciplined", ""),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "ICE BREAKERS",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    AppWidget.getYourContainer("Family", ""),
                    AppWidget.getYourContainer("Cricket", ""),
                    AppWidget.getYourContainer("Single Malts", ""),
                  ],
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  children: [
                    Image.asset("images/twitter.png"),
                    SizedBox(
                      width: 35.0,
                    ),
                    Column(
                      children: [
                        Text(
                          "Loving the thoughtful #personalCRM \n@getbeyond.ai\n TODAY",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset("images/linkedin.png"),
                    Column(
                      children: [
                        Text(
                          "Not just a Personal CRM, it's a Personal\nstory \nYESTERDAY",
                          style: AppWidget.simpleTextFeildStyle("Black"),
                        )
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 40.0,
              ),
              Text(
                "DURING MEETING",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AppWidget.getYourContainer("Greet formally", ""),
                  AppWidget.getYourContainer("Use bullet points", ""),
                ],
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "WHEN CLOSING",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 30.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AppWidget.getYourContainer("Mention Impact", ""),
                  AppWidget.getYourContainer("Summarize", ''),
                  AppWidget.getYourContainer("Set Goals", ""),
                ],
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "AVOID",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              SizedBox(
                height: 30.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  AppWidget.getYourContainer("Emojis", "red"),
                  AppWidget.getYourContainer("Push too hard", "red"),
                  AppWidget.getYourContainer("Being Negative", "red"),
                ],
              ),
              SizedBox(
                height: 40.0,
              ),
              Text(
                "SELECT MOMENTS",
                style: AppWidget.simpleTextFeildStyle("Black"),
              ),
              Divider(
                color: Colors.white,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Team Catch up",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                  Text(
                    "UPCOMING EVENT- FRIDAY - 7:30PM",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    "Sudip <> Naveen",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                  Text("FIRST EVENT - TODAY",
                      style: AppWidget.simpleTextFeildStyle("Black")),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    "NOTES",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  Text(
                    "Kid - Sunday",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "Wife - Subhra",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  Text(
                    "BIO",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    " + Spent over 22 years with fast-growing\n businesses across international markets.",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    " + Founded 2 companies",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Founder & CEO",
                    style: AppWidget.boldTextFeildStyle("Black"),
                  ),
                  Text(
                    "GETBEYOND.AI | 2022-PRESENT",
                    style: AppWidget.simpleTextFeildStyle("Black"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
