import 'package:flutter/material.dart';
import 'package:getbeyond/model/group_model.dart';
import 'package:getbeyond/service/shared_pref.dart';
import 'package:getbeyond/widget/group_widget.dart';
import 'package:getbeyond/widget/widget_support.dart';

class Group extends StatefulWidget {
  Group({Key? key}) : super(key: key);

  @override
  State<Group> createState() => _GroupState();
}

class _GroupState extends State<Group> {
  String? getheme;
  gethisoncall() async {
    getheme = await SharedPreferenceHelper().getUserId();
    setState(() {});
  }

  doonthislaunch() async {
    await gethisoncall();
    setState(() {});
  }

  @override
  void initState() {
    doonthislaunch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: getheme == "Black" ? Colors.black : Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20.0,
              ),
              AppWidget.createSocialMediaButton("", "New Group", getheme!),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "GROUPS NOT PART OF",
                style: AppWidget.simpleTextFeildStyle(getheme!),
              ),
              Divider(
                color: Colors.white,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        getheme == "Black"
                            ? Text("Co-investing(Angel)",
                                style: AppWidget.boldTextFeildStyle("Black"))
                            : Text(
                                "Co-investing(Angel)",
                                style: AppWidget.boldTextFeildStyle("White"),
                              ),
                        getheme == "Black"
                            ? Text("32 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("Black"))
                            : Text(
                                "32 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("White"),
                              )
                      ],
                    ),
                    Icon(
                      Icons.add,
                      color: Colors.blue,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        getheme == "Black"
                            ? Text("Weekday drinks",
                                style: AppWidget.boldTextFeildStyle("Black"))
                            : Text(
                                "Weekday drinks",
                                style: AppWidget.boldTextFeildStyle("White"),
                              ),
                        getheme == "Black"
                            ? Text("04 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("Black"))
                            : Text(
                                "04 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("White"),
                              )
                      ],
                    ),
                    Icon(
                      Icons.add,
                      color: Colors.blue,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "GROUPS PART OF",
                style: AppWidget.simpleTextFeildStyle(getheme!),
              ),
              Divider(
                color: Colors.white,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        getheme == "Black"
                            ? Text("Co-investing(Angel)",
                                style: AppWidget.boldTextFeildStyle("Black"))
                            : Text(
                                "Co-investing(Angel)",
                                style: AppWidget.boldTextFeildStyle("White"),
                              ),
                        getheme == "Black"
                            ? Text("32 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("Black"))
                            : Text(
                                "32 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("White"),
                              )
                      ],
                    ),
                    Icon(
                      Icons.close,
                      color: Colors.red,
                    )
                  ],
                ),
              ),
              SizedBox(height: 20.0),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        getheme == "Black"
                            ? Text("Weekday drinks",
                                style: AppWidget.boldTextFeildStyle("Black"))
                            : Text(
                                "Weekday drinks",
                                style: AppWidget.boldTextFeildStyle("White"),
                              ),
                        getheme == "Black"
                            ? Text("04 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("Black"))
                            : Text(
                                "04 MEMBERS",
                                style: AppWidget.simpleTextFeildStyle("White"),
                              )
                      ],
                    ),
                    Icon(
                      Icons.close,
                      color: Colors.red,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
