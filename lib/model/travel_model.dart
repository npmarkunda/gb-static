import 'package:flutter/cupertino.dart';

class TravelModel {
  late String image;
  late String name;
  late String company;
  late String time;
  late Icon icon;
}
