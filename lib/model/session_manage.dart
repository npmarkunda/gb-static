import 'package:getbeyond/service/shared_pref.dart';

class SessionManagement {
  late String token;
  getToken(String displayToken) async {
    token = displayToken;
    await setToken();
  }

  setToken() {
    SharedPreferenceHelper().saveUserToken(token);
  }
}
